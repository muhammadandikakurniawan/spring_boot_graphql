package com.graphqldemo;

import java.util.UUID;

import javax.sql.DataSource;

import com.graphqldemo.Entities.User;
import com.graphqldemo.Repositories.Interfaces.IUserRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.*;
import org.springframework.transaction.support.TransactionTemplate;

@SpringBootTest
public class UserRepositoryTests {
    
    private static final Propagation Propagation = null;

	@Autowired
    private IUserRepository _userRepo;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private DataSource _ds;

    @Test
    void TestInsert(){
        String id = UUID.randomUUID().toString().replace("-", "");
        User data = new User();
        data.setId(id);
        data.setEmail("dika@gmail.com");
        data.setName("muhammad andika kurniawan".toUpperCase());
        data.setPassword("muhammad andika kurniawan".toUpperCase());

        var res = this._userRepo.save(data);

        String dbg = "";
    }

    @Test
    // @Transactional(rollbackFor = { Exception.class }, transactionManager = "TransactionManager", propagation = org.springframework.transaction.annotation.Propagation.REQUIRED)
    void TestInsertTransactional1(){
        Boolean isRollback = false;
        String id1 = UUID.randomUUID().toString().replace("-", "");

        User data1 = new User();
        data1.setId(id1);
        data1.setEmail("dika@gmail.com");
        data1.setName("muhammad andika kurniawan".toUpperCase());
        data1.setPassword("dika123".toUpperCase());

        String id2 = UUID.randomUUID().toString().replace("-", "");

        User data2 = new User();
        data2.setId(id2);
        data2.setEmail("awan@gmail.com");
        data2.setName("awankun 321".toUpperCase());
        data2.setPassword("awan123".toUpperCase());

        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(status -> {
            
            try{
                this._userRepo.save(data1);
                this._userRepo.save(data2);
                if(isRollback){
                    status.setRollbackOnly();
                }
            }
            catch(DataIntegrityViolationException ex){
                status.setRollbackOnly();
            }
            catch(Exception ex){
                status.setRollbackOnly();
            }
            
            return 1;
        });



        String dbg = "";
    }

}
