package com.graphqldemo.Controllers;

import com.graphqldemo.GraphQlServices.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import graphql.ExecutionResult;

@RestController
@RequestMapping("rest/User")
public class UserServiceController {

    @Autowired
    UserService _userServiceGraphQL;

    @PostMapping
    public ResponseEntity<Object> getAllUser(@RequestBody String query){
        ExecutionResult execute  = _userServiceGraphQL.getGraphQL().execute(query);
        return new ResponseEntity<>(execute, HttpStatus.OK);
    }
    
}
