package com.graphqldemo.GraphQlServices.DataFechers;

import com.graphqldemo.Entities.User;
import com.graphqldemo.Repositories.Interfaces.IUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class GetUserByIdDataFetcher implements DataFetcher<User> {

    @Autowired
    private IUserRepository _UserRepository;

    @Override
    public User get(DataFetchingEnvironment dataFetchingEnvironment) {
        String isn = dataFetchingEnvironment.getArgument("Id");
        return this._UserRepository.findById(isn).get();
    }
    
}
