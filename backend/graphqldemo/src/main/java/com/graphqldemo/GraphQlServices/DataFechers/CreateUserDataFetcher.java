package com.graphqldemo.GraphQlServices.DataFechers;

import java.util.Map;
import java.util.UUID;

import com.graphqldemo.Entities.User;
import com.graphqldemo.Repositories.Interfaces.IUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import graphql.language.Field;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class CreateUserDataFetcher implements DataFetcher<User> {

    @Autowired
    private IUserRepository _UserRepository;

    @Override
    public User get(DataFetchingEnvironment dataFetchingEnvironment) {
        Map data = dataFetchingEnvironment.getArgument("input");
        String name = data.get("Name").toString();
        String email = data.get("Email").toString();
        String password = data.get("Password").toString();
        
        
        String id = UUID.randomUUID().toString().replace("-", "");
        User model = new User(email, name, password);
        model.setId(id);
        return this._UserRepository.save(model);

    }


    
}
