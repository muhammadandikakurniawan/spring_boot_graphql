package com.graphqldemo.GraphQlServices.DataFechers;

import java.util.*;

import com.graphqldemo.Entities.User;
import com.graphqldemo.Repositories.Interfaces.IUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import graphql.schema.DataFetcher;

@Component
public class UserDataFetcher {

    @Autowired
    private IUserRepository _UserRepository;
    
    public DataFetcher<User> CreateUser(){
        return dataFetchingEnvironment -> {
            Map data = dataFetchingEnvironment.getArgument("input");
            String name = data.get("Name").toString();
            String email = data.get("Email").toString();
            String password = data.get("Password").toString();
            
            
            String id = UUID.randomUUID().toString().replace("-", "");
            User model = new User(email, name, password);
            model.setId(id);
            return this._UserRepository.save(model);
        };
    }

    public DataFetcher<List<User>> GetAllUser(){
        return dataFetchingEnvironment -> {
            return this._UserRepository.findAll();
        };
    }

    public DataFetcher<User> GetUserById(){
        return dataFetchingEnvironment -> {
            String id = dataFetchingEnvironment.getArgument("Id");
            return this._UserRepository.findById(id).get();
        };
    }

}
