package com.graphqldemo.GraphQlServices;

import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import com.graphqldemo.GraphQlServices.DataFechers.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.*;

@Service
public class UserService {
    
    @Value("classpath:user_schema.graphqls")
    Resource _resource;

    private GraphQL graphQL;

    @Autowired
    private GetAllUserDataFetcher _getAllUserDataFetcher;
    @Autowired
    private GetUserByIdDataFetcher _getUserByIdDataFetcher;
    @Autowired
    private CreateUserDataFetcher _createUserDataFetcher;

    @Autowired
    private UserDataFetcher _UserDataFetcher;

    // load schema at application start up
    @PostConstruct
    private void loadSchema() throws IOException {

        // get the schema
        File schemaFile = _resource.getFile();
        // parse schema
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(schemaFile);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    private RuntimeWiring buildRuntimeWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher("getAllUser", _UserDataFetcher.GetAllUser())
                        .dataFetcher("getUserById", _UserDataFetcher.GetUserById()))
                .type("Mutation", typeWiring -> typeWiring
                        .dataFetcher("createUser", _UserDataFetcher.CreateUser()))
                .build();
    }


    public GraphQL getGraphQL() {
        return graphQL;
    }

}
