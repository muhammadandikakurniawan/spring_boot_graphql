package com.graphqldemo.GraphQlServices.DataFechers;

import java.util.*;

import com.graphqldemo.Entities.User;
import com.graphqldemo.Repositories.Interfaces.IUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class GetAllUserDataFetcher implements DataFetcher<List<User>> {

    @Autowired
    private IUserRepository _UserRepository;

    @Override
    public List<User> get(DataFetchingEnvironment dataFetchingEnvironment) {
        return this._UserRepository.findAll();
    }

}
