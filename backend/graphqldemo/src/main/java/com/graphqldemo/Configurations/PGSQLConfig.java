package com.graphqldemo.Configurations;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = "com.graphqldemo.Repositories.Interfaces",
		entityManagerFactoryRef = "EntityManager", 
		transactionManagerRef = "TransactionManager"
		)
public class PGSQLConfig {
    
    @Primary
	@Bean
	public DataSource dataSource() {
	  DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setDriverClassName("org.postgresql.Driver");
      dataSource.setUrl("jdbc:postgresql://localhost:5432/db_training");
      dataSource.setUsername("postgres");
      dataSource.setPassword("admin");

      return dataSource;
    }
    
    @Primary
    @Bean(name="EntityManager")
    public LocalContainerEntityManagerFactoryBean EntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(this.dataSource());
        em.setPackagesToScan(new String[] { "com.graphqldemo.Entities" });
 
        HibernateJpaVendorAdapter vendorAdapter= new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setPersistenceUnitName("Pu");
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.connection.shutdown",true);
        properties.put("spring.jpa.show-sql",true);
        properties.put("spring.jpa.hibernate.ddl-auto", "create");
        // properties.put("hibernate.hbm2ddl.auto","update");
        properties.put("hibernate.dialect","org.hibernate.dialect.PostgreSQLDialect");
        properties.put("hibernate.naming.physical-strategy","org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl");
        em.setJpaPropertyMap(properties);
        return em;
    }
    
    @Primary
    @Bean
    public PlatformTransactionManager TransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(EntityManager().getObject());
        return transactionManager;
    }
    
    @Bean
    @Autowired
    public JdbcTemplate jdbcTemplate(DataSource datasouce) {
    	return new JdbcTemplate(datasouce);
    }

}
