package com.graphqldemo.Entities;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Entity
@Table(name = "User", schema = "spring_graphql")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    public User (String email, String name, String password){
        this.Email = email;
        this.Name = name;
        this.Password = password;
    }

    public User (){

    }

    @Id
    private String Id;

    @Column(name="Name", length = 100, nullable = false)
    @JsonProperty("Name")
    private String Name;

    @Column(name="Email", length = 100, nullable = false)
    @JsonProperty("Email")
    private String Email;

    @Column(name="Password", length = 100, nullable = false)
    @JsonProperty("Password")
    private String Password;
    
}
