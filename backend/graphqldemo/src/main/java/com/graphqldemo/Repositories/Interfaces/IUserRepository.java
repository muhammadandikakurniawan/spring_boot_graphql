package com.graphqldemo.Repositories.Interfaces;

import com.graphqldemo.Entities.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends JpaRepository<User,String>{
    
}
